engine = pdflatex
file = relatorio
compile = $(engine) $(file)

all:
	$(compile) >/dev/null; \
	bibtex $(file) >/dev/null; \
	$(compile) >/dev/null; \
	$(compile) >/dev/null;

debug:
	$(compile); \
	bibtex $(file); \
	$(compile); \
	$(compile);

.PHONY: clean
clean:
	rm -f *.aux *.bbl *.bcf *.blg *.log *.out *.dvi *.run.xml *.toc *.nav *.snm *.synctex.gz-current;

